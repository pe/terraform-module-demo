# Input variable definitions

variable "vm_name" {
  description = "name of vm"
  type        = string
}

variable "vm_size" {
  description = "size of vm"
  type        = string
  default     = "1"
}
