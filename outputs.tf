# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

# Output variable definitions

output "resultback" {
  description = "result"
  value       = "result-${var.vm_name}-${var.vm_size}-${random_id.example.hex}"
}

output "out1" {
  description = "output1"
  value       = "out_value1-${random_id.example.hex}"
}

output "out2" {
  description = "output2"
  value       = "out_value2-${random_id.example.hex}"
}

output "out3" {
  description = "output3"
  value       = "out_value3-${random_id.example.hex}"
}
